<?php

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('redirect', function (Request $request) {
    $request->session()->put('state', $state = Str::random(40));

    $query = http_build_query([
        'client_id' => '5',
        'redirect_uri' => 'http://192.241.144.151:8090/callback',
        'response_type' => 'code',
        'state' => $state,
    ]);

    return redirect('http://slta.antyrasolutions.com/resources/oauth/authorize?' . $query);
})->name('sso');


Route::get('/callback', function (Request $request) {
    $state = $request->session()->pull('state');

    throw_unless(
        strlen($state) > 0 && $state === $request->state,
        InvalidArgumentException::class
    );


    $http = new GuzzleHttp\Client;

    $response = $http->post('http://slta.antyrasolutions.com/resources/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '5',
            'client_secret' => 'fBzkUFVkQ0CcSNCxz1kG5UtVY6rfBCPPkbaJLXSz',
            'redirect_uri' => 'http://192.241.144.151:8090/callback',
            'code' => $request->code,
        ],
    ]);

    $access_token = json_decode((string) $response->getBody(), true)['access_token'];

    $authorization = 'Bearer '.$access_token;

    $user_response = $http->request('GET', 'http://slta.antyrasolutions.com/resources/api/auth/user', [
        'headers' => [
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json',
            'Authorization' => $authorization
        ]
    ]);

    return view('profile')->with(['data' => json_decode((string) $user_response->getBody(), true)]);
});

