<!doctype html>
<html lang="en">
  <head>
    <title>User Details</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>

    <div class="container mt-5 pt-5">
     <div class="row">
         <div class="col-md-6">
             <div class="card">
                 <div class="card-body">
                     <h5 class="card-title">User Details</h5>
                     <p class="card-text">

                        <b>user_name</b> : {{$data['data']['info']['user_name']}}<br>

                        <b>title</b> : {{$data['data']['info']['name']['title']}}<br>

                        <b>first_name</b> : {{$data['data']['info']['name']['first_name']}}<br>

                        <b>last_name</b> : {{$data['data']['info']['name']['last_name']}}<br>

                        <b>email</b> : {{$data['data']['info']['email']}}<br>

                        <b>mobile_no</b> : {{$data['data']['info']['mobile_no']}}<br>

                        <b>job_title</b> : {{$data['data']['job_title']}}<br>

                        <b>signing_type</b> : {{$data['data']['signing_type']}}<br>

                        <b>category</b> : {{$data['data']['category']}}<br>

                        <b>category_other</b> : {{$data['data']['category_other']}}<br>

                        <b>company_name</b> : {{$data['data']['company_name']}}<br>

                        <b>is_authorized</b> : {{$data['data']['is_authorized']}}<br>

                        <b>licensed</b> : {{$data['data']['licensed']}}<br>

                        <b>license_type</b> : {{$data['data']['license_type']}}<br>

                        <b>brand_name</b> : {{$data['data']['brand_name']}}<br>

                        <b>province</b> : {{$data['data']['province']}}<br>
                     </p>
                 </div>
             </div>
         </div>
         <div class="col-md-6">
            <code>{{ var_dump($data) }}</code>
         </div>
     </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    {{-- <script>
        var data = JSON.parse({{ json_encode($data) }});
        document.getElementById("json").innerHTML = JSON.stringify(data, undefined, 2);
    </script> --}}

</body>
</html>
